# Generated by Django 3.2.3 on 2021-06-05 02:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0002_auto_20210604_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='vertical',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='students.vertical'),
        ),
    ]
